# TagSignature

This is a very simple script that displays the 'signature' of a tag when you hover the mouse over that tag.  Only works if Vim `has('balloon_eval')` and if there is an available tag file. 

Released versions are available on the [vim scripts page][vimscripts].

Here are some screenshots:

![One][shot1]

![Two][shot2]

![Three][shot3]

![Four][shot4]

[vimscripts]: http://www.vim.org/scripts/script.php?script_id=2714
[shot1]: https://sites.google.com/site/abudden/contents/Vim-Scripts/tag-balloons/tagsignature_dark.jpg
[shot2]: https://sites.google.com/site/abudden/contents/Vim-Scripts/tag-balloons/tagsignature_light.jpg
[shot3]: https://sites.google.com/site/abudden/contents/Vim-Scripts/tag-balloons/mouse_dark.png
[shot4]: https://sites.google.com/site/abudden/contents/Vim-Scripts/tag-balloons/mouse_light.png
